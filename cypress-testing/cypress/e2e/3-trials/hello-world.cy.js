/// <reference types="cypress" />

describe('trials',() => {
    beforeEach(() => {
        cy.visit('https://codedamn.com')
        cy.viewport('iphone-xr')

        // cy.then(() => {
        //     window.localStorage.setItem('__auth__token', token)
        })

    it('Login page looks good', () => {
        cy.contains('Learn').should('exist')
        // cy.get('div#root').should('exist')
        // cy.get('div#noroot').should('not.exist')
        // cy.get('.asyncComponent > div > a')
        // const el = cy.get('[data-testid=menutoogle]')
        // expect(el.text() === 'hello')

        // cy.contains('Sign In').click()
        // cy.contains('Login to your account').should('exist')
        // cy.contains('Go with Google').should('exist')
        // cy.contains('Go with Facebook').should('exist')

        cy.contains('Forgot Password?').click()
        cy.url().should('include', '/password-reset')
        cy.log('Current URL =', cy.url())
        cy.go('back')
    })

  it('Login should display correct error', () => {
    cy.contains('Sign In').click()

    cy.contains('Unable to authorize').should('not.exist')
    
    cy.get('[data-testid=username]').type('admin')
    cy.get('[data-testid=password]').type('admin')
    
    cy.get('[data-testid="login"]').click()

    cy.contains('Unable to authorize').should('exist')
  })  
})